
module.exports = function(grunt){
    grunt.loadNpmTasks('grunt-contrib-connect');


    grunt.initConfig({
        __:{
            srcDir: 'app'
        },
        connect: connectCfg(grunt)
    })

    grunt.registerTask('serve', ['connect:server'])
    grunt.registerTask('default', ['serve'])
}




function connectCfg(grunt){
    return {
        options: {
            keepalive: true
        },
        server: {
            hostname: '0.0.0.0',
            base: ['src', 'bower_components', 'node_modules']
        }
    }
}