#Angular Material Playground

####Browser Compatibility Note

> "....Angular Material is targeted for all browsers with versions n-1; where n is the current browser version."
source: https://github.com/angular/material/tree/v1.1.1


####Run the project
`grunt serve`

####Access the available examples
- /src/sidebar/index.html